CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended Modules
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

Drupal helper module for development.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


RECOMMENDED MODULES
-------------------

No recommeded.


INSTALLATION
------------

Install the optimizely module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
--------------

No configuration.


MAINTAINERS
-----------

The 8.x branches were created by:

 * Thao Huynh (zipme_hkt) - https://www.drupal.org/u/zipme_hkt
