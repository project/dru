<?php

namespace Drupal\dru\Helper;

use Drupal\Core\Entity\ContentEntityType;

/**
 * Class Entity.
 *
 * @package Drupal\dru\Helper
 */
class Entity extends HelperBase {

  /**
   * Get entity type options.
   *
   * @return array
   *   Array of entity types.
   */
  public function getEntityTypeOptions() {
    $content_entity_types = [];
    $entity_type_definations = $this->getEntityTypeManager()->getDefinitions();
    /* @var $definition EntityTypeInterface */
    foreach ($entity_type_definations as $definition) {
      if ($definition instanceof ContentEntityType) {
        $content_entity_types[$definition->id()] = $definition->getLabel();
      }
    }
    return $content_entity_types;
  }

}
