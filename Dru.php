<?php

/**
 * Dru helper for Drupal development.
 */
class Dru extends Drupal {

  /**
   * Registry storage.
   *
   * @var array
   */
  static private $registry = [];

  /**
   * Returns the currently active global container.
   *
   * @return \Symfony\Component\DependencyInjection\ContainerInterface|null
   *   The result container.
   *
   * @throws \Drupal\Core\DependencyInjection\ContainerNotInitializedException
   */
  public static function getContainer() {
    return \Drupal::getContainer();
  }

  /**
   * Returns TRUE if the container has been initialized, FALSE otherwise.
   *
   * @return bool
   *   Check result.
   */
  public static function hasContainer() {
    if (static::$container == NULL) {
      static::$container = \Drupal::getContainer();
    }
    return static::$container !== NULL;
  }

  /**
   * Register a new variable.
   *
   * @param string $key
   *   The key.
   * @param mixed $value
   *   The value.
   * @param bool $override
   *   The override flag.
   */
  public static function register($key, $value, $override = FALSE) {
    if (isset(self::$registry[$key])) {
      if (!$override) {
        return;
      }
    }
    self::$registry[$key] = $value;
  }

  /**
   * Unregister a variable from register by key.
   *
   * @param string $key
   *   The key.
   */
  public static function unregister($key) {
    if (isset(self::$registry[$key])) {
      if (is_object(self::$registry[$key]) && (method_exists(self::$registry[$key], '__destruct'))) {
        self::$registry[$key]->__destruct();
      }
      unset(self::$registry[$key]);
    }
  }

  /**
   * Retrieve a value from registry by a key.
   *
   * @param string $key
   *   The key.
   *
   * @return mixed
   *   Register result.
   */
  public static function registry($key) {
    if (isset(self::$registry[$key])) {
      return self::$registry[$key];
    }
    return NULL;
  }

  /**
   * Return data if isset or default.
   *
   * @param mixed|array $var
   *   The check variable.
   * @param bool|false $default
   *   Default value.
   *
   * @return bool|array|mixed
   *   Get result if isset.
   */
  public static function issetOr(&$var, $default = FALSE) {
    return isset($var) ? $var : $default;
  }

  /**
   * Check variable isset and equal with data.
   *
   * @param mixed|array $var
   *   The variable.
   * @param mixed $data
   *   The data.
   *
   * @return bool
   *   Check result.
   */
  public static function issetEqual(&$var, $data) {
    return isset($var) ? $var == $data : FALSE;
  }

  /**
   * Check variable isset and set value.
   *
   * @param mixed $var
   *   The variable.
   * @param mixed $value
   *   The value.
   */
  public static function setValueIsset(&$var, $value) {
    if (isset($var)) {
      $var = $value;
    }
  }

  /**
   * Determine if a given string starts with a given substring.
   *
   * @param string $haystack
   *   Input string.
   * @param string|array $needles
   *   Check string or array.
   *
   * @return bool
   *   Check result.
   */
  public static function startsWith($haystack, $needles) {
    foreach ((array) $needles as $needle) {
      if ($needle != '' && substr($haystack, 0, strlen($needle)) === (string) $needle) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Determine if a given string ends with a given substring.
   *
   * @param string $haystack
   *   Input string.
   * @param string|array $needles
   *   Check string or array.
   *
   * @return bool
   *   Check result.
   */
  public static function endsWith($haystack, $needles) {
    foreach ((array) $needles as $needle) {
      if (substr($haystack, -strlen($needle)) === (string) $needle) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Determine if a given string contains a given substring.
   *
   * @param string $haystack
   *   Input string.
   * @param string|array $needles
   *   Check string or array.
   *
   * @return bool
   *   Check result.
   */
  public static function contains($haystack, $needles) {
    foreach ((array) $needles as $needle) {
      if ($needle != '' && mb_strpos($haystack, $needle) !== FALSE) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Retrieves a helper service.
   *
   * @param string $id
   *   The ID of the service to retrieve.
   *
   * @return mixed
   *   The specified service.
   */
  public static function helper($id) {
    return static::service('dru_helper.' . $id);
  }

  /**
   * Retrieves a form helper service.
   *
   * @return mixed
   *   The specified service.
   */
  public static function formHelper() {
    return self::helper('form');
  }

  /**
   * Retrieves a language helper service.
   *
   * @return mixed
   *   The specified service.
   */
  public static function languageHelper() {
    return self::helper('language');
  }

  /**
   * Retrieves a node helper service.
   *
   * @return mixed
   *   The specified service.
   */
  public static function nodeHelper() {
    return self::helper('node');
  }

  /**
   * Retrieves a taxonomy helper service.
   *
   * @return mixed
   *   The specified service.
   */
  public static function taxonomyHelper() {
    return self::helper('taxonomy');
  }

  /**
   * Retrieves a user helper service.
   *
   * @return mixed
   *   The specified service.
   */
  public static function userHelper() {
    return self::helper('user');
  }

  /**
   * Retrieves a field helper service.
   *
   * @return mixed
   *   The specified service.
   */
  public static function fieldHelper() {
    return self::helper('field');
  }

}
